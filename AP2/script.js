window.onload = () => {
    mostrarJogador()
}


function mostrarJogador(){
    let box = document.querySelector('.boxDetalhes')
    let jogador = document.cookie.split('$')    
    box.innerHTML = `
        <img src='${jogador[2]}'>
        <h2>${jogador[0]}</h2>
        <h3>${jogador[1]}</h3>
        <h4>${jogador[3]}</h4>
        <h5>${jogador[4]}</h5>
        <h6>${jogador[5]}</h6>
        <button onclick="voltar()">Voltar</button>`;
}

function voltar(){
    document.cookie = ''

    window.location.href = '../../';
}
